<?php

class Dice {
    private  $faces;
    private  $freqs = array();
    private  $probability;
    
    // Constructor
    public function __construct($faces,$probability) {
        $this->faces = $faces;
        $this->probability =$probability;
    }
    
    public function cast() {
        $res;
        if(empty($this->probability)){
        $res = rand(1,$this->faces);
        $this->freqs[$res]++;
        return $res;
        }else{
            if(rand(0,1000)<=($this->probability*1000)){
                $res=$this->faces;
                $this->freqs[$res]++;
                return $res;
            }else{
                $res = rand(1,$this->faces-1);
                $this->freqs[$res]++;
                return $res;
            }
            
            
        }
    }
    
    public function getFreq($eyes) {
        $freq = $this->freqs[$eyes];
        if ($freq=="")
            $freq = 0;
        return $freq;
    }
    public function average(){
        $sum=0;
        $throws=0;
        for($i=1;$i<=$this->faces;$i++){
            for($j=1;$j<=$this->getFreq($i);$j++){
            $sum += $i;
            $throws++;
            }
        }
        if($throws==0){
        
        $throws=1;    
        
        }
        return $sum/$throws;
    }
}
class PhysicalDice extends Dice{
    private $material;
    
    public function _construct($faces,$probability,$material){
        parent::__construct($faces,$probability);
        $this->material=$material;
        
    }
    public function getMaterial(){
         return $material;
        
    }
     
}
               

?>

