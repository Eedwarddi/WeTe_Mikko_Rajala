<?php

include("diceclasses.inc.php");

$faces = $_GET["faces"];
$throws = $_GET["throws"];
$probability = $_GET["probability"];
$material = $_GET["material"];
$results = array();

// make dice
$dice = new PhysicalDice($faces,$probability,$material);
for ($i = 1; $i<=$throws; $i++) {
    $res = $dice->cast();
    $results[] = array('id' => strval($i), 'res' => strval($res));
}
$freqs = array();
for ($i = 1; $i<=$faces; $i++) {
    $freqs[] = array ('eyes' => strval($i), 'frequency' => strval($dice->getFreq($i)));
}
echo json_encode(array('faces'=>$faces,'material' => $material,'results'=>$results,'frequencies'=>$freqs,'average'=>$dice->average()));



?>