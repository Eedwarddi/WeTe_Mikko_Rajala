var notes = (function() {
	var list = [];
	return {
		add: function(note) {
			if(/\S/.test(note)&& typeof note === 'string'){
				var item = {timestamp: Date.now(), text: note};
				list.push(item);
				return true;
			}
			return false;
		},
		remove: function(index) {
			if(index > this.count() -1 || index == null){
				return false;
			}
			list.splice(index, 1);
			return true;
		},
		count: function() {
			return list.length;
		},
		list: function() {
			return list;
		},
		find: function(str) {
			var found = [];
			for(i = 0; i < list.length; i++){
				if(list[i].text.includes(str)){
					found.push(list[i]);
				}
			}
			return found;
		},
		clear: function() {
			list.splice(0, list.length);
		}
	};
}());